package rover;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MoveForwardTest {

    @Test
    void shouldMoveForward(){
        Position position = new Position(0,0,CardinalDirection.N);

        Command command = new MoveForward();
        Position result = command.action(position);
        Position expectPosition = new Position(0,1,CardinalDirection.N);
        assertEquals(expectPosition.toString(), result.toString());

    }
    @Test
    void shouldMoveForwardWhenFacingEast(){
        Position position = new Position(0,0,CardinalDirection.E);

        Command command = new MoveForward();
        Position result = command.action(position);
        Position expectPosition = new Position(1,0,CardinalDirection.E);
        assertEquals(expectPosition.toString(), result.toString());

    }

    @Test
    void shouldMoveForwardWhenNotAtOrigin(){
        Position position = new Position(1,1,CardinalDirection.N);

        Command command = new MoveForward();
        Position result = command.action(position);
        Position expectPosition = new Position(1,2,CardinalDirection.N);
        assertEquals(expectPosition.toString(), result.toString());

    }
}
