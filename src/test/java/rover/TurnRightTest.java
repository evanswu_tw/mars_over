package rover;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TurnRightTest {

    @Test
    void shouldTurnRight(){

        Position position = new Position(0,0,CardinalDirection.N);

        Command command = new TurnRight();

        Position result = command.action(position);
        Position expectPosition = new Position(0,0,CardinalDirection.E);
        assertEquals(expectPosition.toString(), result.toString());
    }
}
