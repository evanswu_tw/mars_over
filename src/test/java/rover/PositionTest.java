package rover;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;



import static org.junit.jupiter.api.Assertions.assertEquals;

public class PositionTest {

    private Position position;

    @BeforeEach
    public void startup(){
        position = new Position(0,0, CardinalDirection.N);
    }

    @Test
    public void shouldGetOrientation(){
        assertEquals(CardinalDirection.N, position.getOrientation());
    }

    @Test
    public void shouldLookPretty()
    {
        assertEquals("0 0 N", position.toString());
    }




}
