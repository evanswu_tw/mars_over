package rover;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TurnLeftTest {

    @Test
    void shouldTurnLeft(){

        Position position = new Position(0,0,CardinalDirection.N);

        Command command = new TurnLeft();

        Position result = command.action(position);
        Position expectPosition = new Position(0,0,CardinalDirection.W);
        assertEquals(expectPosition.toString(), result.toString());
    }
}
