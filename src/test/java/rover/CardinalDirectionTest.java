package rover;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CardinalDirectionTest {

    @Test
    public void shouldReturnWestWhenTurningLeftFromNorth() {
        assertEquals(CardinalDirection.N.left(), CardinalDirection.W);
    }
    @Test
    public void shouldReturnEastTurningRightFromNorth() {
        assertEquals(CardinalDirection.N.right(), CardinalDirection.E);
    }

}