package rover;

public class MoveForward implements Command {

    @Override
    public Position action(Position originalPos) {
        switch (originalPos.getOrientation()) {
            case N:
                return new Position(originalPos.getX(), originalPos.getY() + 1, CardinalDirection.N);
            case E:
                return new Position(originalPos.getX() + 1, originalPos.getY(), CardinalDirection.E);
            case S:
                return new Position(originalPos.getX(), originalPos.getY() - 1, CardinalDirection.S);
            case W:
                return new Position(originalPos.getX() - 1, originalPos.getY(), CardinalDirection.W);
            default:
                return null;
        }
    }
}
