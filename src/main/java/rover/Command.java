package rover;

public interface Command {
    Position action(Position originalPos);
}
