package rover;

public class Position {
    private CardinalDirection orientation;
    private int x;
    private int y;


    Position(int x, int y, CardinalDirection orientation) {
        this.x = x;
        this.y = y;
        this.orientation = orientation;
    }

    public CardinalDirection getOrientation() {
        return orientation;
    }


    @Override
    public String toString() {
        return x + " " + y + " " + getOrientation();
    }

    public int getY() {
        return y;
    }


    public int getX() {
        return x;
    }


}
