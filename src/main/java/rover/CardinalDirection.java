package rover;

public enum CardinalDirection {
    N,
    E,
    S,
    W;

    public CardinalDirection left() {
        return getCardinalDirection(W, N, E, S);
    }

    public CardinalDirection right() {
        return getCardinalDirection(E, S, W, N);
    }

    private CardinalDirection getCardinalDirection(CardinalDirection e, CardinalDirection s, CardinalDirection w, CardinalDirection n) {
        switch (this) {
            case N:
                return e;
            case E:
                return s;
            case S:
                return w;
            case W:
                return n;
            default:
                return null;
        }
    }

}
