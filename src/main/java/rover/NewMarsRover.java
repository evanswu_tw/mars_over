package rover;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NewMarsRover {

    private static final List<String> VALID_COMMANDS = Arrays.asList("L", "R", "M");
    private Position position;


    public NewMarsRover(int startingX, int startingY, String direction) {
        position = new Position(startingX, startingY, CardinalDirection.valueOf(direction));
    }

    private Map<String, Command> initCommandMap() {
        Map<String, Command> commandMap = new HashMap<>();
        commandMap.put("L", new TurnLeft());
        commandMap.put("R", new TurnRight());
        commandMap.put("M", new MoveForward());
        return commandMap;
    }

    public String run(String input) {
        String[] commands = convertInputIntoCommands(input);
        Map<String, Command> commandMap = initCommandMap();
        Command command;
        for (String commandString : commands) {
            command = commandMap.get(commandString);
            position = command.action(position);
        }
        return position.toString();
    }

    private static String[] convertInputIntoCommands(String input) {
        String[] commandArray = input.split("(?!^)");
        validateCommands(input, commandArray);
        return commandArray;
    }

    private static void validateCommands(String input, String[] commandArray) {
        for (String command : commandArray) {
            if (!VALID_COMMANDS.contains(command)) {
                throw new IllegalArgumentException("Invalid command sequence: " + input);
            }
        }
    }
}
