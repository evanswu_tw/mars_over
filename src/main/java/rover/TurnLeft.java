package rover;

public class TurnLeft implements Command{

    @Override
    public Position action(Position originalPos) {
        return new Position(originalPos.getX(),originalPos.getY(), originalPos.getOrientation().left());
    }
}
