package rover;

public class TurnRight implements Command {
    @Override
    public Position action(Position originalPos) {
        return new Position(originalPos.getX(),originalPos.getY(), originalPos.getOrientation().right());
    }
}
